﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLanguage : MonoBehaviour
{
    [SerializeField] GameLanguge gameLanguage;
    public GameLanguge GameLanguage { get => gameLanguage; set => gameLanguage = value; }


    public delegate void UserSelectLanguageDel(SelectLanguage selectLanguage);
    public static event UserSelectLanguageDel UserSelectLanguageEvent;

    private void OnMouseDown()
    {
        if(UserSelectLanguageEvent != null)
        {
            UserSelectLanguageEvent(this);
        }
    }
}
