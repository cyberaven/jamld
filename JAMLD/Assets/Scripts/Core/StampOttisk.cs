﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StampOttisk : MonoBehaviour
{
    StampColor stampColor;   
    public StampColor StampColor { get => stampColor; set => stampColor = value; }

    [SerializeField] Image view;
    [SerializeField] Sprite red;
    [SerializeField] Sprite green;

    
    private void Start()
    {
        StateMashine.GameStateChangeEvent += DestroyStampOttisk;

        if(stampColor == StampColor.Red)
        {
            view.GetComponent<Image>().sprite = red;
        }
        if (stampColor == StampColor.Green)
        {
            view.GetComponent<Image>().sprite = green;
        }
    }
    private void OnDestroy()
    {
        StateMashine.GameStateChangeEvent -= DestroyStampOttisk;
    }

    private void DestroyStampOttisk(StateMashine s)
    {
        if(s.CurrentGameState == GameState.StartTurn)
        {
            Destroy(this.gameObject);
        }
    }




}
