﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class District : MonoBehaviour
{
    [SerializeField] Fraction fraction;
    public Fraction Fraction { get => fraction; set => fraction = value; }

    [SerializeField] House house;

    [SerializeField] List<House> houses = new List<House>();

    private void Start()
    {
        PowerRes.PowerResTextChangeEvent += ResChange;

        for (int i = 0; i < 25; i++)
        {
            CreateNewHouse(fraction);            
        }
    }
    private void OnDestroy()
    {
        PowerRes.PowerResTextChangeEvent -= ResChange;
    }
    private void ResChange(PowerRes res)
    {
        if(res.Fraction == fraction)
        {
            int resValue = res.CurrentValue;
            int hC = houses.Count;
            int delta = resValue - hC;

            if(delta > 0)
            {
                AddHouses(delta);
            }
            else
            {
                RemoveHouses(delta);
            }
        }
    }
   private void CreateNewHouse(Fraction fraction)
    {
        House h = Instantiate(house, transform);
        h.transform.localPosition = RandomLocalPosition();
        h.Fraction = fraction;
        houses.Add(h);
    }
    private Vector3 RandomLocalPosition()
    {
        Vector3 newPos = Vector3.zero;
        float dWidth = GetComponent<RectTransform>().rect.width/2;
        float dHeight = GetComponent<RectTransform>().rect.height/2;
        float randWidth = UnityEngine.Random.Range(-dWidth, dWidth);
        float randHeight = UnityEngine.Random.Range(-dHeight, dHeight);
        newPos = new Vector3(randWidth, randHeight, 0);
        return newPos;
    }    
    private void AddHouses(float delta)
    {
        for (int i = 0; i < delta; i++)
        {
            CreateNewHouse(fraction);
        }
    }
    private void RemoveHouses(float delta)
    {
        List<House> newHousesList = new List<House>();
        float x = houses.Count + delta;

        for (int i = 0; i < x-1; i++)
        {            
            newHousesList.Add(houses[i]);
        }
        for (float a = x; a < houses.Count; a++)
        {
            Destroy(houses[(int)a].gameObject);
        }
        houses = newHousesList;
    }
}
