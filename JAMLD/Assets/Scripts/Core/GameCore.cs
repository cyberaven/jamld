﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCore : MonoBehaviour
{
    public static GameCore INSTANCE;

    [SerializeField] StateMashine stateMashine;
    public StateMashine StateMashine { get => stateMashine; set => stateMashine = value; }
    

    [SerializeField] List<GameEvent> gameEvents;
    [SerializeField] GameEvent currentGameEvent;

    [SerializeField] int baseResValue = 25;    
    public int BaseResValue { get => baseResValue; set => baseResValue = value; }

    [SerializeField] int winVallue = 50;
    public int WinVallue { get => winVallue; set => winVallue = value; }

    public GameEvent CurrentGameEvent { get => currentGameEvent; set => currentGameEvent = value; }
    
    [SerializeField] private GameLanguge gameLanguаge = GameLanguge.RU;
    public GameLanguge GameLanguage { get => gameLanguаge; set => gameLanguаge = value; }



    private void Awake()
    {
        INSTANCE = this;
        stateMashine = Instantiate(stateMashine, transform);

        AllEventsCompletedOff();

        StateMashine.GameStateChangeEvent += GameStateChange;
        RestartButton.RestartGameEvent += RestartGame;
        SelectLanguage.UserSelectLanguageEvent += UserSelectLanguage;
    }
    private void OnEnable()
    {
        ShowNewEvent();
    }
    
    private void OnDestroy()
    {
        StateMashine.GameStateChangeEvent -= GameStateChange;
        RestartButton.RestartGameEvent -= RestartGame;
        SelectLanguage.UserSelectLanguageEvent -= UserSelectLanguage;
    }
    
    private void GameStateChange(StateMashine s)
    {
        ShowNewEvent();
    }

    private void ShowNewEvent()
    {
        if (stateMashine.CurrentGameState == GameState.StartTurn)
        {
            DelCurrentEvent();

            List<GameEvent> notCompletedEvents = SelectNotCompletedEvents();
            if(notCompletedEvents.Count == 0)
            {
                GameEnd();
            }
            else
            {
                currentGameEvent = Instantiate(notCompletedEvents[Random.Range(0, notCompletedEvents.Count)]);
            }
            
        }
    }
    private List<GameEvent> SelectNotCompletedEvents()
    {
        List<GameEvent> notCompletedEvents = new List<GameEvent>();
        foreach (GameEvent ge in gameEvents)
        {            
            if (ge.GameEventData.Completed == Completed.No)
            {
                notCompletedEvents.Add(ge);
            }
        }
        return notCompletedEvents;
    }
    private void DelCurrentEvent()
    {
        if(currentGameEvent != null)
        {
            Destroy(currentGameEvent.gameObject);
        }
    }
    private void GameEnd()
    {
        Debug.Log("GameEnds");
    }
    private void AllEventsCompletedOff()
    {
        foreach (GameEvent ge in gameEvents)
        {
            ge.GameEventData.Completed = Completed.No;
        }
    }
    private void RestartGame()
    {
        AllEventsCompletedOff();
    }
    private void UserSelectLanguage(SelectLanguage s)
    {
        gameLanguаge = s.GameLanguage;
    }
}
