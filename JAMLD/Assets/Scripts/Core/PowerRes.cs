﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerRes : MonoBehaviour
{
    [SerializeField] Fraction fraction;
    public Fraction Fraction { get => fraction; set => fraction = value; }
   
    [SerializeField] GameEvent currentGameEvent;
    public GameEvent CurrentGameEvent { get => currentGameEvent; set => currentGameEvent = value; }
    
    int winValue;
    int baseValue;
    int currentValue;
    public int CurrentValue { get => currentValue; set => currentValue = value; }


    [SerializeField] Scrollbar scrollbar;

    public delegate void PowerResTextChangeDel(PowerRes pr);
    public static event PowerResTextChangeDel PowerResTextChangeEvent;

    public delegate void FractionWinGame(Fraction fraction);
    public static event FractionWinGame FractionWinGameEvent;

    public delegate void FractionLoseGame(Fraction fraction);
    public static event FractionLoseGame FractionLoseGameEvent;


    private void OnEnable()
    {   
        if (GameCore.INSTANCE != null)
        {
            currentGameEvent = GameCore.INSTANCE.CurrentGameEvent;
            baseValue = GameCore.INSTANCE.BaseResValue;
            winValue = GameCore.INSTANCE.WinVallue;
            CurrentValue = baseValue;

            ChangeScrollbarSize();
        }
    }
    private void Start()
    {       
        RestartButton.RestartGameEvent += RestartGame;
        GameEvent.NewGameEventEvent += NewGameEvent;
        Stamp.PushOnStampEvent += PushOnStamp;
    }
    private void OnDestroy()
    {
        RestartButton.RestartGameEvent -= RestartGame;
        GameEvent.NewGameEventEvent -= NewGameEvent;
        Stamp.PushOnStampEvent -= PushOnStamp;
    }
    private void NewGameEvent(GameEvent ge)
    {
        currentGameEvent = ge;      
    }
    private void PushOnStamp(Stamp s)
    {      
        if(currentGameEvent == null)
        {
            currentGameEvent = GameCore.INSTANCE.CurrentGameEvent;
        }
        if (s.StampColor == StampColor.Red)
        {
            if (fraction == Fraction.Angel)
            {
                CurrentValue += currentGameEvent.GameEventData.NegativeAngel;
            }
            else if (fraction == Fraction.Demon)
            {
                CurrentValue += currentGameEvent.GameEventData.NegativeDemon;
            }
            else if (fraction == Fraction.Neutral)
            {
                CurrentValue += currentGameEvent.GameEventData.NegativeNeutral;
            }
            else if (fraction == Fraction.Player)
            {
                CurrentValue += currentGameEvent.GameEventData.NegativePlayer;
            }
        }
        if (s.StampColor == StampColor.Green)
        {
            if (fraction == Fraction.Angel)
            {
                CurrentValue += currentGameEvent.GameEventData.PositiveAngel;
            }           
            else if (fraction == Fraction.Neutral)
            {
                CurrentValue += currentGameEvent.GameEventData.PositiveNeutral;
            }
            else if (fraction == Fraction.Player)
            {
                CurrentValue += currentGameEvent.GameEventData.PositivePlayer;
            }
            else if (fraction == Fraction.Demon)
            {
                if(currentGameEvent == null)
                {
                    currentGameEvent = GameCore.INSTANCE.CurrentGameEvent;
                }
                CurrentValue += currentGameEvent.GameEventData.PositiveDemon;
            }
        }
        CheckWinLoseValue();
        ChangeScrollbarSize();

        if (PowerResTextChangeEvent != null)
        {
            PowerResTextChangeEvent(this);
        }       
    }
    private void CheckWinLoseValue()
    {       
        if (CurrentValue >= winValue)
        {
            Debug.Log("WinGame");

            AudioManager.INSTANCE.Win();

            if(FractionWinGameEvent != null)
            {
                FractionWinGameEvent(fraction);
            }
        }

        if (CurrentValue <= 0)
        {
            Debug.Log("LoseGame");

            AudioManager.INSTANCE.Lose();

            if (FractionLoseGameEvent != null)
            {
                FractionLoseGameEvent(fraction);
            }
        }
    }
    private void RestartGame()
    {
        CurrentValue = GameCore.INSTANCE.BaseResValue;
    }
    private void ChangeScrollbarSize()
    {     
        scrollbar.size = ((CurrentValue * 100f)/winValue)/100f;       
    }
}
