﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Arrow : MonoBehaviour
{
    [SerializeField] Image view;
    [SerializeField] Sprite red;
    [SerializeField] Sprite green;

    [SerializeField] Fraction fraction;
    public Fraction Fraction { get => fraction; set => fraction = value; }

    GameEvent currentGameEvent;

    private void Start()
    {
        gameObject.SetActive(false);
        GameEvent.NewGameEventEvent += NewGameEvent;
        Stamp.TakeStampInHandEvent += TakeStampOnHend;
        Stamp.StampOnStartPosEvent += PutStampOnStartPos;
    }
    private void OnDestroy()
    {
        GameEvent.NewGameEventEvent -= NewGameEvent;
        Stamp.TakeStampInHandEvent -= TakeStampOnHend;
        Stamp.StampOnStartPosEvent -= PutStampOnStartPos;
    }

    private void NewGameEvent(GameEvent ge)
    {
        currentGameEvent = ge;
    }
    private void TakeStampOnHend(Stamp s)
    {
        gameObject.SetActive(true);

        if(currentGameEvent == null)
        {
            currentGameEvent = GameCore.INSTANCE.CurrentGameEvent;
        }

        if(s.StampColor == StampColor.Green)
        {
            if (fraction == Fraction.Angel)
            {
                if (currentGameEvent.GameEventData.PositiveAngel >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }                
            }
            if (fraction == Fraction.Demon)
            {
                if (currentGameEvent.GameEventData.PositiveDemon >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
            if (fraction == Fraction.Neutral)
            {
                if (currentGameEvent.GameEventData.PositiveNeutral >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
            if (fraction == Fraction.Player)
            {
                if (currentGameEvent.GameEventData.PositivePlayer >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
        }
        if (s.StampColor == StampColor.Red)
        {
            if (fraction == Fraction.Angel)
            {
                if (currentGameEvent.GameEventData.NegativeAngel >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
            if (fraction == Fraction.Demon)
            {
                if (currentGameEvent.GameEventData.NegativeDemon >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
            if (fraction == Fraction.Neutral)
            {
                if (currentGameEvent.GameEventData.NegativeNeutral >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
            if (fraction == Fraction.Player)
            {
                if (currentGameEvent.GameEventData.NegativePlayer >= 0)
                {
                    view.GetComponent<Image>().sprite = green;
                }
                else
                {
                    view.GetComponent<Image>().sprite = red;
                }
            }
        }
    }
    private void PutStampOnStartPos()
    {
        gameObject.SetActive(false);
    }
}
