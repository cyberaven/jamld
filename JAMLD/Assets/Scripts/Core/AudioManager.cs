﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
    public static AudioManager INSTANCE;

    [SerializeField] AudioClip music;
    [SerializeField] AudioClip stamp;
    [SerializeField] AudioClip win;
    [SerializeField] AudioClip lose;

    AudioSource audioSource;

    private void Awake()
    {
        INSTANCE = this;

        audioSource = GetComponent<AudioSource>();
    }

    public void Stamp()
    {
        audioSource.PlayOneShot(stamp);
    }
    public void Win()
    {
        audioSource.PlayOneShot(win);
    }
    public void Lose()
    {
        audioSource.PlayOneShot(lose);
    }
    public void Music()
    {
        audioSource.clip = music;
        audioSource.loop = true;
        audioSource.Play(0);       
    }

}
