﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum StampColor
{
    Red,
    Green
}
public enum FollowCursor
{
    Yes,
    No
}

public class Stamp : MonoBehaviour
{
    FollowCursor followCursor = FollowCursor.No;

    [SerializeField] StampColor stampColor;
    public StampColor StampColor { get => stampColor; set => stampColor = value; }

    [SerializeField] StampOttisk stampOttisk;   

    public delegate void PushOnStampDel(Stamp s);
    public static event PushOnStampDel PushOnStampEvent;

    public delegate void TakeStampInHand(Stamp s);
    public static event TakeStampInHand TakeStampInHandEvent;

    public delegate void StampOnStartPos();
    public static event StampOnStartPos StampOnStartPosEvent;

    private void Update()
    {
        MoveWithCursor();
        RightMouseClk();
    }
    private void OnMouseDown()
    {
        if (GameCore.INSTANCE.StateMashine.CurrentGameState == GameState.Event)
        {
            if (followCursor == FollowCursor.No)
            {
                followCursor = FollowCursor.Yes;

                if(TakeStampInHandEvent != null)
                {
                    TakeStampInHandEvent(this);
                }
            }
            else
            {
                followCursor = FollowCursor.No;

                PutOttisk();
                PutStampOnStartPos();               

                Debug.Log("На меня нажали " + stampColor.ToString());
                if (PushOnStampEvent != null)
                {
                    PushOnStampEvent(this);
                }
            }
        }
    }
    private void MoveWithCursor()
    {
        if(followCursor == FollowCursor.Yes)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        }
    }

    private void PutOttisk()
    {
        AudioManager.INSTANCE.Stamp();

        StampOttisk s = Instantiate(stampOttisk, GameUI.INSTANCE.transform);       
        s.StampColor = stampColor;
        s.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        s.transform.position = new Vector3(transform.position.x, transform.position.y, 0);       
    }
    private void RightMouseClk()
    {
        if(Input.GetMouseButtonDown(1))
        {
            followCursor = FollowCursor.No;
            PutStampOnStartPos();
        }
    }
    private void PutStampOnStartPos()
    {
        RectTransform rt = GetComponent<RectTransform>();
        rt.localPosition = Vector3.zero;

        if(StampOnStartPosEvent != null)
        {
            StampOnStartPosEvent();
        }
    }
}
