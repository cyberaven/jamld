﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class House : MonoBehaviour
{
    Fraction fraction;

    public Fraction Fraction { get => fraction; set => fraction = value; }

    [SerializeField] Image view;

    [SerializeField] Sprite angel;
    [SerializeField] Sprite demon;
    [SerializeField] Sprite neutral;
    [SerializeField] Sprite player;

    private void Start()
    {
         if(fraction == Fraction.Angel)
        {
            view.GetComponent<Image>().sprite = angel;
        }
        if (fraction == Fraction.Demon)
        {
            view.GetComponent<Image>().sprite = demon;
        }
        if (fraction == Fraction.Neutral)
        {
            view.GetComponent<Image>().sprite = neutral;
        }
        if(fraction == Fraction.Player)
        {
            view.GetComponent<Image>().sprite = player;

        }
    }
}
