﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Localizator : MonoBehaviour
{
    private Text text;

    private void Start()
    {
        text = GetComponent<Text>();

        Localization();
    }

    private void Localization()
    {       
        text.text = LocalizationData.INSTANCE.SearchLocalization(text.text);
    }   
}
