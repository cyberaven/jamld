﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum GameLanguge
{
    RU,
    ENG
}


public class LocalizationData : MonoBehaviour
{
    public static LocalizationData INSTANCE;

    [SerializeField] List<LocalizationRow> localizationRows;

    private void Awake()
    {
        INSTANCE = this;
    }

    public string SearchLocalization(string text)
    {
        string result = "";

        result = CompareRow(text);

        if (result == "" || result == null)
        {
            result = text;
        }
        return result;
    }

    private string CompareRow(string searcStr)
    {
        string result = "";

        GameLanguge gL = GameCore.INSTANCE.GameLanguage;

        foreach (LocalizationRow lR in localizationRows)
        {
            if (gL == GameLanguge.RU)
            {
                if (lR.engRow == searcStr)
                {
                    result = lR.ruRow;
                }
            }
            if (gL == GameLanguge.ENG)
            {
                if (lR.engRow == searcStr)
                {
                    result = lR.engRow;
                }
            }
        }
        return result;
    }
}

