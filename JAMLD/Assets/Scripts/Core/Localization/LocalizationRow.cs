﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "LocalizationRow", menuName = "LocalizationRow", order = 51)]
public class LocalizationRow : ScriptableObject
{
    public int id = 0;
    public string ruRow = "";
    public string engRow = "";
}
