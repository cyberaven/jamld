﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinGameScreenPanel : Panel
{
    [SerializeField] Fraction fraction;
    public Fraction Fraction { get => fraction; set => fraction = value; }

    [SerializeField] Image view;
    [SerializeField] Sprite angel;
    [SerializeField] Sprite demon;
    [SerializeField] Sprite neutral;
    [SerializeField] Sprite player;

    [SerializeField] Text text;
    [SerializeField] string angelTxt;
    [SerializeField] string demonTxt;
    [SerializeField] string neutralTxt;
    [SerializeField] string playerTxt;

    private void Start()
    {
        if(fraction == Fraction.Angel)
        {
            view.GetComponent<Image>().sprite = angel;
            text.text = angelTxt;
        }
        if (fraction == Fraction.Demon)
        {
            view.GetComponent<Image>().sprite = demon;
            text.text = demonTxt;
        }
        if (fraction == Fraction.Neutral)
        {
            view.GetComponent<Image>().sprite = neutral;
            text.text = neutralTxt;
        }
        if (fraction == Fraction.Player)
        {
            view.GetComponent<Image>().sprite = player;
            text.text = playerTxt;
        }
    }

}
