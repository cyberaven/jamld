﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MainMenuPanel : Panel
{
    [SerializeField] Button startGameBtn;    

    public delegate void StartGameBtnClkDel();
    public static event StartGameBtnClkDel StartGameBtnClkEvent;

    void Start()
    {
        startGameBtn.onClick.AddListener(StartGameBtnClk);

        AudioManager.INSTANCE.Music();
    }
    private void StartGameBtnClk()
    {
        if(StartGameBtnClkEvent != null)
        {
            StartGameBtnClkEvent();
        }
    }

    public void Restart()
    {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }
}
