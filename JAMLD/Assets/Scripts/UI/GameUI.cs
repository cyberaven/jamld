﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    public static GameUI INSTANCE;

    [SerializeField] MainMenuPanel mainMenuPanel;
    [SerializeField] WinGameScreenPanel winGameScreenPanel;
    [SerializeField] LoseGameScreenPanel loseGameScreenPanel;
    [SerializeField] GameScreenPanel gameScreenPanel;


    [SerializeField] GameObject resPanel;
    [SerializeField] GameObject leftHead;
    [SerializeField] GameObject rightHead;
    [SerializeField] GameObject dialogView;
    [SerializeField] GameObject redStampView;
    [SerializeField] GameObject greenStampView;
    [SerializeField] GameObject cityView;    

    private void Awake()
    {
        INSTANCE = this;
        MainMenuPanel.StartGameBtnClkEvent += StartGame;
        PowerRes.FractionWinGameEvent += WinGame;       
        PowerRes.FractionLoseGameEvent += LoseGame;
        RestartButton.RestartGameEvent += RestartGame;
    }
    private void OnDestroy()
    {
        MainMenuPanel.StartGameBtnClkEvent -= StartGame;
        PowerRes.FractionWinGameEvent -= WinGame;
        PowerRes.FractionLoseGameEvent -= LoseGame;
        RestartButton.RestartGameEvent -= RestartGame;
    }

    private void Start()
    {
        mainMenuPanel.Show();
    }
    public void HideAll()
    {
        mainMenuPanel.Hide();
        winGameScreenPanel.Hide();
        loseGameScreenPanel.Hide();
        gameScreenPanel.Hide();
    }
    private void StartGame()
    {
        gameScreenPanel.Show();
    }
    private void WinGame(Fraction fraction)
    {
        winGameScreenPanel.Show();
        winGameScreenPanel.Fraction = fraction;
    }
    private void LoseGame(Fraction fraction)
    {
        loseGameScreenPanel.Show();
        loseGameScreenPanel.Fraction = fraction;
    }
    private void RestartGame()
    {
        mainMenuPanel.Show();
    }
}
