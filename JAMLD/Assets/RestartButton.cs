﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestartButton : MonoBehaviour
{

    public delegate void RestartGameDel();
    public static event RestartGameDel RestartGameEvent;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(Clk);
    }

    private void Clk()
    {
        GameCore.INSTANCE.StateMashine.CurrentGameState = GameState.StartTurn;
        RestartGame();
    }
    private void RestartGame()
    {
        if(RestartGameEvent != null)
        {
            RestartGameEvent();
        }
    }
}
